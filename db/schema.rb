# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "clients", primary_key: "client_id", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "client_name", limit: 40, null: false
    t.string "client_surname", limit: 40
    t.string "contact_num", limit: 15, null: false
    t.string "email_address", limit: 50, null: false
    t.string "address", limit: 50, null: false
  end

  create_table "vehicles", primary_key: "engine_number", id: :integer, default: nil, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "make", limit: 30, null: false
    t.string "model", limit: 30, null: false
    t.integer "manufacture_year", null: false
    t.integer "chasis_number", null: false
    t.integer "cubic_capacity", null: false
    t.decimal "vehicle_value", precision: 8, scale: 2
    t.integer "client_id"
    t.index ["client_id"], name: "client_id"
  end

  add_foreign_key "vehicles", "clients", primary_key: "client_id", name: "vehicles_ibfk_1", on_delete: :nullify
end
