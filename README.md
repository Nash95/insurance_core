# insurance_core

## Steps to configure the app for use
1. The application is a rails restful API so make sure you have ruby and rails framework installed on your local machine first, if not please follow the following link or any other for steps to install: https://www.tutorialspoint.com/ruby-on-rails/rails-installation.htm .
2. Then clone the *insurance_core* repository onto your local machine.
3. Make sure you have *mySQL* installed on your local machine and then create a local instance of the database.
4. Inside the cloned project files, open config/database.yml file and configure the database to your local instance.
5. Run rails db:migrate in your terminal to create tables in the database.
6. From terminal/gitbash from your local machine in the project's directory and run bundle install to pull dependancies for the project.
7. Start the server.
8. After running the application, the rails api will be running on url "http://localhost:3000/" .
9. The *JSON* files for the tables are found on the following routes respectively :

- http://localhost:3000/clients
- http://localhost:3000/vehicles

10. Please visit the link below to clone the Angular client and begin insurance processing https://gitlab.com/Nash95/insurance_angular_client 
11. Kindly note that the android client is still under development.

